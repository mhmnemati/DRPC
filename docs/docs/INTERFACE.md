# Application Interface

## Library API

### DRPC extends EventEmitter

* __Function__: `drpc.{Command}(params)`
* __Info__: Sync function
* __Commands__:

    | Command        | Description                   |
    | -------------- | ----------------------------- |
    | __Start__      | Start service                 |
    | __Stop__       | Stop service                  |
    | __Connect__    | Connect to machine            |
    | __Disconnect__ | Disconnect from machine       |
    | __List__       | List of connected machines    |
    | __Broadcast__  | Broadcastmessage to machines  |
    | __Multicast__  | Multicast message to machines |
    | __Unicast__    | Unicast message to a machine  |

* __Events__: `drpc.on(event, handler)`:

    | Event          | Handler Params   | Description                        |
    | -------------- | ---------------- | ---------------------------------- |
    | __start__      | ()               | Service started                    |
    | __error__      | (error)          | Service errored                    |
    | __stop__       | ()               | Service stopped                    |
    | __connect__    | (host, port, name) | Service connected a machine        |
    | __disconnect__ | (host, port, name) | Service disconnected a machine     |
    | __cast__       | (name, message)  | A cast received from a machine     |

#### new DRPC()

* __params__: `(port, name, password)`

    | Parameter    | Value Type  | Description                 |
    | ------------ | ----------- | --------------------------- |
    | __port__     | __integer__ | Service listener port       |
    | __name__     | __string__  | Service name                |
    | __password__ | __string__  | Service connection password |

* __result__: DRPC

#### Start

* __params__: `()`

    | Parameter    | Value Type  | Description                 |
    | ------------ | ----------- | --------------------------- |

* __result__: void

#### Stop

* __params__: `()`

    | Parameter    | Value Type  | Description                 |
    | ------------ | ----------- | --------------------------- |

* __result__: void

#### Connect

* __params__: `(host, port, password)`

    | Parameter    | Value Type  | Description             |
    | ------------ | ----------- | ----------------------- |
    | __host__     | __string__  | Target service host     |
    | __port__     | __integer__ | Target service port     |
    | __password__ | __string__  | Target service password |

* __result__: void

#### Disconnect

* __params__:  `(name)`

    | Parameter | Value Type  | Description         |
    | --------- | ----------- | ------------------- |
    | __name__  | __string__  | Target service name |

* __result__: void

#### List

* __params__: `()`

    | Parameter | Value Type  | Description         |
    | --------- | ----------- | ------------------- |

* __result__: list

#### Broadcast

* __params__: `(message)`

    | Parameter   | Value Type        | Description     |
    | ----------- | ----------------- | --------------- |
    | __message__ | __[string,json]__ | Casting message |

* __result__: boolean

#### Multicast

* __params__: `(names, message)`

    | Parameter   | Value Type        | Description            |
    | ----------- | ----------------- | ---------------------- |
    | __names__   | __array[string]__ | Casting services names |
    | __message__ | __[string,json]__ | Casting message        |

* __result__: boolean

#### Unicast

* __params__: `(name, message)`

    | Parameter   | Value Type        | Description          |
    | ----------- | ----------------- | -------------------- |
    | __name__    | __string__        | Casting service name |
    | __message__ | __[string,json]__ | Casting message      |

* __result__: boolean